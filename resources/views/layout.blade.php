<!doctype html>
<html class="no-js" lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        @yield('meta')
        <title>INTERNATIONAL | @yield('title') | Motransa S.A. Ecuador</title>
        <link rel="icon" type="image/png" href="{{ asset('/favicon.png') }}" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @stack('style')
        <!--Start of Zendesk Chat Script-->
        <script type="text/javascript">
          window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
          d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
          _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
          $.src="https://v2.zopim.com/?56xUNtuHRdzBtCexNh7FgOR0Ch1ohsNk";z.t=+new Date;$.
          type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
          $zopim(function() {
              $zopim.livechat.button.setOffsetVertical(39);
              $zopim.livechat.window.setSize('medium');
              $zopim.livechat.window.setTitle('Motransa');
          });
        </script>
        <!--End of Zendesk Chat Script-->
    </head>
    <body>

        @include('_partials.svg')

        <div id="app">
          <header class="header">
              <div class="container">
                  <a href="/" class="header-logo"><img src="{{ asset('images/international-logo-white.svg') }}" alt=""></a>
                  <div class="header-responsive-icon" @click="menu = !menu" :class="{ 'header-responsive-icon--active': menu }">
                      <svg><use xlink:href="#menu" /></svg>
                  </div>
                  <nav class="header-nav" v-show="menu">
                      <li><a href="/nosotros">NOSOTROS</a></li>
                      <li><a href="/camiones">CAMIONES</a></li>
                      <li><a href="/repuestos">REPUESTOS</a></li>
                      <li><a href="/servicio-tecnico">SERVICIO</a></li>
                      <li><a href="/nosotros#featured">CONCESIONARIOS</a></li>
                  </nav>
                  @if(Auth::user())
                    <form action="/logout" class="login-button login-button--active tippy" title="Cerrar sesión" method="POST">
                        {{ csrf_field() }}
                        <button>
                            <img src="{{ Avatar::create(Auth::user()->name)->setDimension(500)->setFontSize(200)->setBackground('#101629')->toBase64() }}" alt="">
                        </button>
                    </form>
                  @else 
                    <a href="/login" class="login-button tippy" title="Iniciar sesión"><svg><use xlink:href="#user" /></svg></a>
                  @endif
                  <a href="/" class="header-logo"><img src="{{ asset('images/motransa-logo.svg') }}" alt=""></a>
              </div>
          </header>

        @include('composers.cartbutton')

          @yield('content')
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>