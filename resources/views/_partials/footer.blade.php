<footer class="footer">
    <div class="container">
        <p class="copyright"><b>© 2016 MOTRANSA.</b> TODOS LOS DERECHOS RESERVADOS | <a href="http://www.geniale.ec/" target="_blank" title="Desarrollo web"><b>Geniale</b></a></p>
        <ul class="footer-icons">
            <li><a href="https://www.facebook.com/internationaltrucks/?fref=ts" target="_blank"><svg><use xlink:href="#facebook" /></svg></a></li>
            <li><a href="https://twitter.com/intnltrucks" target="_blank"><svg><use xlink:href="#twitter" /></svg></a></li>
            <li><a href="https://www.youtube.com/user/Internationaltruck" target="_blank"><svg><use xlink:href="#youtube" /></svg></a></li>
        </ul>
    </div>
</footer>