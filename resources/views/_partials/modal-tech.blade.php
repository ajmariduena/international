<div class="modal-overlay">
	<div class="modal">
		<h1>SOLICITAR CITA DE SERVICIO TÉCNICO</h1>
		<div class="close-modal" onclick="closeModal()"><svg><use xlink:href="#close" /></svg></div>
		<div class="modal-grid">
			<div class="modal-col">
				{!! Form::open(array('name' => 'cotFrm', 'url' => 'cotizar-ser', 'method' => 'POST')) !!}
					<input name="compania" type="text" placeholder="Nombre de compañía">
					<input name="nombre" type="text" placeholder="* Nombre y Apellido">
					<input name="correo" type="email" placeholder="* Correo electrónico">
					<input name="telefono" type="text" placeholder="* Teléfono">
					<select name="adicional1" id="">
						<option value="" selected disabled>* Concesionario</option>
						<option value="Quito">Quito</option>
						<option value="Guayaquil">Guayaquil</option>
					</select>
					<textarea name="adicional2" id="" placeholder="Mensaje"></textarea>
					<button type="button" onclick="validarCot();">Solicitar</button>
				{!! Form::close() !!}
			</div>
			<div class="modal-col">
				<img src="{{ asset('images/servicio.jpg') }}" alt="">
			</div>
		</div>
	</div>
</div>
