<head>
    <link rel="stylesheet" href="{{ asset('css/boostrap.css') }}">
    <style>
        * {
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <div>
        <img src="{{ asset('images/header-pdf.png') }}" style="width:100%;display:block;margin-bottom:24px;">
        <div class="container" style="padding:0;">
            <div class="row" style="margin-top:16px;">
                <div style="padding-left:0;width:300px;display:inline-block;" class="pull-left">
                    <b>R.U.C.:</b> {{ env('RUC') }}
                </div>
                <div style="background:#EBE9E6;padding:16px;width:300px;display:inline-block;" class="pull-right">
                    <div style="margin-bottom:14px;line-height:1.5;">
                        <span class="d-block"><b>QUITO</b></span><br>
                        Av. 10 de Agosto 6398 y Juan de Azcaray, sector la Y PBX: (02) 294-0800 FAX: (02) 294-0800 Ext:2030
                        Av. Maldonado S60-295 y Calle S60 Telef. (02) 3651749
                    </div>
                    <div style="line-height:1.5;">
                        <span class="d-block"><b>GUAYAQUIL</b></span><br>
                        Av. Juan Tanca Marengo Km 1 1/2 y Av. Francisco Orellana PBX: (04) 224-8666 FAX: (04) 223-3977
                    </div>
                </div>
            </div>
            <hr class="bg-orange border-0 h-px w-full" style="border-color:#f0802c;">
            <table class="row table" style="border:0;">
                <tr>
                    <td style="border:none;">
                        <div class="col-md-6 pull-left" style="background:#EBE9E6;margin-right:6px;padding:16px;">
                            <table class="table" style="background:none;margin-bottom:0;">
                                <tr class="mb-2" style="line-height:1.5;">
                                    <td style="border:none;"><small><b>R.U.C. / C.I.</b></small></td>
                                    <td style="border:none;"><small>{{ $user->legal_id }}</small></td>
                                </tr>
                                <tr class="mb-2" style="line-height:1.5;">
                                    <td style="border:none;"><small><b>Cliente</b></small></td>
                                    <td style="border:none;"><small>{{ $user->name }}</small></td>
                                </tr>
                                <tr class="mb-2" style="line-height:1.5;">
                                    <td style="border:none;"><small><b>Direcci&oacute;n</b></small></td>
                                    <td style="border:none;"><small>{{ $user->address }}</small></td>
                                </tr>
                                <tr class="mb-2" style="line-height:1.5;">
                                    <td style="border:none;"><small><b>Ciudad</b></small></td>
                                    <td style="border:none;"><small>{{ $user->city }}<</small></td>
                                </tr>
                                <tr style="line-height:1.5;">
                                    <td style="border:none;"><small><b>Telef/Celular</b></small></td>
                                    <td style="border:none;"><small>{{ $user->phone }}</small></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="border:none;">
                        <div class="col-md-6 pull-right" style="background:#EBE9E6;margin-left:6px;padding:16px;">
                            <h5 class="font-family:Helvetica;">COTIZACI&Oacute;N REPUESTOS</h5>
                            <table style="line-height:1.5;width:100%;margin-bottom:0; background:none;" class="table" >
                                <tr>
                                    <td style="border:none;"><small><b>Fecha de Cotizaci&oacute;n</b></small></td>
                                    <td style="border:none;"><span>{{ $now->format('Y-m-d') }}</span></td>
                                </tr>
                                <tr>
                                    <td style="border:none;">
                                        <small><b>Cotizaci&oacute;n</b></small>
                                    </td>
                                    <td style="border:none;">
                                        <small>100002522</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:none;">
                                        <small><b>Fecha de Vencimiento</b></small>
                                    </td>
                                    <td style="border:none;">
                                        <small>17 de Octubre del 2015</small>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </td>
            </table>
            <br><br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width:100px"><small>Cant.</small></th>
                        <th class="text-center"><small>Descripci&oacute;n</small></th>
                        <th class="text-center" style="width:150px"><small>Precio Unitario</small></th>
                        <th class="text-center" style="width:150px"><small>Precio Total</small></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cart->content() as $item)
                        <tr class="text-center">
                            <td class="py-2">{{ $item->qty }}</td>
                            <td class="py-2">{{ $item->name }}</td>
                            <td class="py-2">{{ $item->price }}</td>
                            <td class="py-2">{{ $item->subtotal }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                <div style="border:2px solid #EBE9E6;width:400px;line-height:1.5;padding:16px;margin-bottom:75px;" class="pull-right">
                    <div>
                        <span style="display:inline-block;width:35%;"><b>Subtotal</b></span>
                        <span style="font-size:14px; width:60%;display:inline-block;text-align:right;">{{ $cart->subtotal() }}</span>
                    </div>
                    <div>
                        <span style="display:inline-block;width:35%;"><b>Valor IVA 12.00 %</b></span>
                        <span style="font-size:14px; width:60%;display:inline-block;text-align:right;">{{ $cart->tax() }}</span>
                    </div>
                    <div>
                        <span style="display:inline-block;width:35%;"><b>Total</b></span>
                        <span style="font-size:14px; width:60%;display:inline-block;text-align:right;">{{ $cart->total() }}</span>
                    </div>
                </div>
            </div>
            <div class="mt-8"><br><br></div>
        </div>
    </div>
</body>