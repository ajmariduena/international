<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="/favicon.png">

    <title>{{ config('app.name') }} - @yield('title')</title>

    <!-- Main styles for this application -->
    <link href="{{ asset('css/cms/app.css') }}" rel="stylesheet">
    @stack('style')
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
        @include('admin._partials.header')
        
        <div class="app-body" id="app">  
            @include('admin._partials.sidebar')
            <!-- Main content -->
            <main class="main">
                @yield('breadcrumb')
                <div class="container-fluid">
                    <div class="">
                        @yield('content')
                    </div>
                </div>
            </main>
        </div>

        @include('admin._partials.footer')

        <!-- Bootstrap and necessary plugins -->
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="https://unpkg.com/popper.js@1.12.5/dist/umd/popper.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
        <script>
            var seo;
        </script>
        @stack('script')
        <script src="{{ asset('js/cms/app.js') }}"></script>
    </body>

</html>
