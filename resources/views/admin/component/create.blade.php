@extends('admin.layouts.app')

@section('breadcrumb')
    {!! Breadcrumbs::render('create-component', $source, Request::get('name')) !!}
@endsection

@section('title', 'Crear Componente')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Crear</strong> {{ Request::get('name') }}
                </div>
                <form method="POST" action="{{ route('components.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-block">
                        <input type="text" name="componentable_id" hidden value="{{ $source->id }}">
                        <input type="text" name="componentable_type" hidden value="{{ 'App\\' . $source->model_name }}">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="title">Título</label>
                                <input type="text" class="form-control @if($errors->has('title')) is-invalid @endif" id="title" name="title" placeholder="Título">
                                @if($errors->has('title'))
                                    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="subtitle">Subtitulo</label>
                                <input type="text" class="form-control @if($errors->has('subtitle')) is-invalid @endif" id="subtitle" name="subtitle" placeholder="Subtitulo">
                                @if($errors->has('subtitle'))
                                    <div class="invalid-feedback">{{ $errors->first('subtitle') }}</div>
                                @endif
                            </div>
                        </div>
                        <input type="text" name="key" hidden value="{{ Request::get('key') }}">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="description">Descripción</label>
                                <textarea class="description form-control @if($errors->has('description')) is-invalid @endif" rows="3" name="content" placeholder="Descripción"></textarea>
                                @if($errors->has('description'))
                                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="logo">Imagen</label>
                                <input-file-preview name="resource" accepted="image/*">
                                </input-file-preview>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{ route("{$source->plural_name}.edit", $source) }}" class="btn btn-default">Volver</a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection