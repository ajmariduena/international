@extends('admin.layouts.app')

@section('breadcrumb')
    {!! Breadcrumbs::render('component', $component->componentable, $component) !!}
@endsection

@section('title', 'Editar Componente')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Editar</strong> Componente
                </div>
                <form method="POST" action="{{ route('components.update', $component) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="card-block">
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('title')) has-danger @endif">
                                <label for="title">Título</label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="Título" value="{{ $component->title }}">
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">{{ $errors->first('title') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('subtitle')) has-danger @endif">
                                <label for="subtitle">Subtitulo</label>
                                <input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Subtitulo" value="{{ $component->subtitle }}">
                                @if($errors->has('subtitle'))
                                    <div class="form-control-feedback">{{ $errors->first('subtitle') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="subtitle">Estado</label>
                                <div>
                                    <label class="switch switch-text switch-pill switch-primary">
                                        <input type="checkbox" name="status" class="switch-input" @if($component->status) checked @endif>
                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('description')) has-danger @endif">
                                <label for="description">Descripción</label>
                                <textarea class="description form-control" rows="3" name="content" placeholder="Descripción">{!! $component->content !!}</textarea>
                                @if($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label for="description">Recursos</label>
                                <drag-n-drop :images="{{ $component->resources }}" name="{{ $component->model_name }}" id="{{ $component->id }}" url="{{ route('resources.store', $component) }}"></drag-n-drop>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{ route("{$component->type_of}.edit", $component->componentable) }}" class="btn btn-default">Volver</a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection