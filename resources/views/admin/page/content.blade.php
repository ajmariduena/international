<div class="col-md-12">
    <div class="row justify-content-sm-between">
        <h3 class="m-0 font-weight-normal">Contenido</h3>
        @if(count($page->creatableComponents))
            <div class="btn-group">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                    Agregar
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach($page->creatableComponents as $creatable)
                        <a class="dropdown-item" href="{{ route('components.create', ['type' => $page->model_name, 'id' => $page->id, 'key' => $creatable->key, 'name' => $creatable->name]) }}">{{ $creatable->name }}</a>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <br>
    <div class="row">
        <table class="table col btn-shadow">
            <thead>
                <tr>
                    <th>Título</th>
                    <th></th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($page->components as $component)
                    <tr>
                        <td>{{ $component->title }}</td>
                        <td>
                            @if(in_array($component->key, $page->creatableComponents->pluck('key')->toArray()))
                                <span class="badge badge-warning">{{ $page->creatableComponents->where('key', $component->key)->first()->name }}</span>
                            @endif
                        </td>
                        <td>
                            @if($component->status)
                                <span class="badge badge-success">Activo</span>
                            @else
                                <span class="badge badge-danger">Inactivo</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('components.edit', $component) }}" class="btn btn-primary btn-sm">Editar</a>
                            <delete route="{{ route('components.destroy', $component) }}" token="{{ csrf_token() }}"></delete>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="m-3"></div>
</div>