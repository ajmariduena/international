@extends('admin.layouts.app')

@section('title', 'Páginas')

@section('breadcrumb')
    {!! Breadcrumbs::render('pages') !!}
@endsection

@section('content')
    <div class="row justify-content-sm-between">
        <h3 class="m-0 font-weight-normal">Páginas</h3>
        <a href="{{ route('pages.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Nueva Página</a>
    </div>
    <br>
    <div class="row">
        <table class="table col btn-shadow">
            <thead>
                <tr>
                <th>#</th>
                <th>Título</th>
                <th>Url</th>
                <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pages as $page)
                    <tr>
                        <th scope="row">{{ $page->id }}</th>
                        <td>{{ $page->title }}</td>
                        <td>{{ $page->slug }}</td>
                        <td>
                            <a href="{{ route('pages.edit', $page) }}" class="btn btn-primary btn-sm">Editar</a>
                            {{--  <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Borrar</button>  --}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        {{ $pages->links() }}
    </div>
@endsection