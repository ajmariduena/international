@extends('admin.layouts.app')

@section('title', 'Crear Página')

@section('breadcrumb')
    {!! Breadcrumbs::render('create-page') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card btn-shadow">
                <div class="card-header">
                    <strong>Crear</strong> Página
                </div>
                <form method="POST" action="{{ route('pages.store') }}">
                    {{ csrf_field() }}
                    <div class="card-block">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="title">Título</label>
                                <input type="text" class="form-control @if($errors->has('title')) is-invalid @endif" id="title" name="title" placeholder="Título">
                                <small class="form-text text-muted">Máximo 70 carácteres.</small>
                                @if($errors->has('title'))
                                    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="description">Descripción</label>
                                <textarea class="form-control @if($errors->has('description')) is-invalid @endif" rows="3" name="description" placeholder="Descripción"></textarea>
                                <small class="form-text text-muted">Máximo 156 carácteres.</small>
                                @if($errors->has('description'))
                                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 col-md-3">
                                <label for="description">Vista</label>
                                <select class="custom-select form-control @if($errors->has('view')) is-invalid @endif" name="view">
                                    <option selected disabled>Seleccionar Vista</option>
                                    @foreach($views as $view)
                                        <option value="{{ $view }}">{{ $view }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('view'))
                                    <div class="invalid-feedback">{{ $errors->first('view') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{ route('pages.index') }}" class="btn btn-default">Volver</a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection