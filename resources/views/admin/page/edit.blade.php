@extends('admin.layouts.app')

@section('title', $page->title)

@section('breadcrumb')
    {!! Breadcrumbs::render('page', $page) !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card btn-shadow">
                <div class="card-header">
                    <strong>Editar</strong> Página
                </div>
                <form method="POST" action="{{ route('pages.update', $page) }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="card-block">
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('title')) has-danger @endif">
                                <label for="title">Título</label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="Título" value="{{ $page->title }}" v-model="seo.name">
                                <small class="form-text text-muted">Máximo 70 carácteres.</small>
                                @if($errors->has('title'))
                                    <div class="form-control-feedback">{{ $errors->first('title') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('subtitle')) has-danger @endif">
                                <label for="subtitle">Url</label>
                                <input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Título" value="{{ $page->slug }}" disabled>
                                @if($errors->has('subtitle'))
                                    <div class="form-control-feedback">{{ $errors->first('subtitle') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('description')) has-danger @endif">
                                <label for="description">Descripción</label>
                                <textarea class="form-control" rows="3" name="description" placeholder="Descripción" v-model="seo.description">{!! $page->description !!}</textarea>
                                <small class="form-text text-muted">Máximo 156 carácteres.</small>
                                @if($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12 @if($errors->has('description')) has-danger @endif">
                                <label for="description">Vista previa</label>
                                <div class="seo-preview">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/2/2f/Google_2015_logo.svg">
                                    <div>
                                        <h3>{{ config('app.name') }} - @{{ seo.name }}</h3>
                                        <span>{{ url('/') . '/' . $page->slug }}</span>
                                        <p>@{{ seoDescription(seo.description) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{ route('pages.index') }}" class="btn btn-default">Volver</a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Page Components -->
    @include('admin.page.content')
@endsection

@push('script')
    <script>
        var seo = {
            name: '{{ $page->title }}',
            description: '{{ $page->description }}'
        }   
    </script>
@endpush