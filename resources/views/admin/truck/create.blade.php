@extends('admin.layouts.app')

@section('title', 'Crear Camión')

@section('breadcrumb')
    {!! Breadcrumbs::render('create-truck') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card btn-shadow">
                <div class="card-header">
                    <strong>Crear</strong> Camión
                </div>
                <form method="POST" action="{{ route('trucks.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-block">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="title">* Nombre</label>
                                <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" placeholder="Nombre del camión" value="{{ old('name') }}">
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                              <label for="description">* Descripción</label>
                              <textarea class="form-control @if($errors->has('description')) is-invalid @endif" id="description" rows="3" name="description" placeholder="Descripción del camión">{{ old('description') }}</textarea>
                              @if($errors->has('description'))
                                  <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                              @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="load_amount">* Capacidad de carga</label>
                                <input type="text" class="form-control @if($errors->has('load_amount')) is-invalid @endif" id="load_amount" name="load_amount" placeholder="ej: 25,000 kg" value="{{ old('load_amount') }}">
                                @if($errors->has('load_amount'))
                                    <div class="invalid-feedback">{{ $errors->first('load_amount') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="title">* Precio</label>
                                <input type="text" class="form-control @if($errors->has('price')) is-invalid @endif" id="price" name="price" placeholder="ej: 60,000" value="{{ old('price') }}">
                                @if($errors->has('price'))
                                    <div class="invalid-feedback">{{ $errors->first('price') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="featured_image">* Imágen principal</label>
                                <input-file-preview name="featured_image" accepted="image/*">
                                </input-file-preview>
                                @if($errors->has('featured_image'))
                                    <div class="invalid-feedback">{{ $errors->first('featured_image') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="specs">Características técnicas</label>
                                <textarea class="description form-control @if($errors->has('specs')) is-invalid @endif" rows="3" name="specs" placeholder="Características técnicas">{!! old('specs') !!}</textarea>
                                @if($errors->has('specs'))
                                    <div class="invalid-feedback">{{ $errors->first('specs') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="specs_image">Imágen para Características técnicas</label>
                                <input-file-preview name="specs_image" accepted="image/*">
                                </input-file-preview>
                                @if($errors->has('specs_image'))
                                    <div class="invalid-feedback">{{ $errors->first('specs_image') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="dimensions">Capacidades y Dimensiones</label>
                                <textarea class="description form-control @if($errors->has('dimensions')) is-invalid @endif" rows="3" name="dimensions" placeholder="Capacidad y dimensiones">{!! old('dimensiones') !!}</textarea>
                                @if($errors->has('dimensions'))
                                    <div class="invalid-feedback">{{ $errors->first('dimensions') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="dimensions_image">Imágen para Capacidades y Dimensiones</label>
                                <input-file-preview name="dimensions_image" accepted="image/*">
                                </input-file-preview>
                                @if($errors->has('dimensions_image'))
                                    <div class="invalid-feedback">{{ $errors->first('dimensions_image') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="accessories">Cabina y accesorios</label>
                                <textarea class="description form-control @if($errors->has('accessories')) is-invalid @endif" rows="3" name="accessories" placeholder="Cabina y accesorios">{!! old('accessories') !!}</textarea>
                                @if($errors->has('accessories'))
                                    <div class="invalid-feedback">{{ $errors->first('accessories') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="accessories_image">Imágen para Cabina y accesorios</label>
                                <input-file-preview name="accessories_image" accepted="image/*">
                                </input-file-preview>
                                @if($errors->has('accessories_image'))
                                    <div class="invalid-feedback">{{ $errors->first('accessories_image') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="catalog">Catálogo</label>
                                <input type="file" class="form-control-file form-control @if($errors->has('catalog')) is-invalid @endif" id="catalog" name="catalog" accept="application/pdf">
                                <small class="form-text text-muted">Catálogo en formato PDF.</small>
                                @if($errors->has('catalog'))
                                    <div class="invalid-feedback">{{ $errors->first('catalog') }}</div>
                                @endif
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="truck_family_id">* Familia</label>
                                <select class="form-control @if($errors->has('truck_family_id')) is-invalid @endif" id="truck_family_id" name="truck_family_id">
                                    @foreach($families as $family)
                                        <option value="{{ $family->id }}">{{ $family->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('truck_family_id'))
                                    <div class="invalid-feedback">{{ $errors->first('truck_family_id') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{ route('trucks.index') }}" class="btn btn-default">Volver</a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection