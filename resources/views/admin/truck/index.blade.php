@extends('admin.layouts.app')

@section('title', 'Camiones')

@section('breadcrumb')
    {!! Breadcrumbs::render('trucks') !!}
@endsection

@section('content')
    <div class="row justify-content-sm-between">
        <h3 class="m-0 font-weight-normal">Camiones</h3>
        <a href="{{ route('trucks.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Nuevo</a>
    </div>
    <br>
    <div class="row">
        <table class="table col btn-shadow">
            <thead>
                <tr>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Familia</th>
                <th>Estado</th>
                <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($trucks as $truck)
                    <tr>
                        <td>{{ $truck->name }}</td>
                        <td>{{ $truck->price }}</td>
                        <td>{{ $truck->family->name }}</td>
                        <td>
                            @if($truck->status)
                                <span class="badge badge-success">Activo</span>
                            @else
                                <span class="badge badge-danger">Inactivo</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('trucks.edit', $truck) }}" class="btn btn-primary btn-sm">Editar</a>
                            <delete route="{{ route('trucks.destroy', $truck) }}" token="{{ csrf_token() }}"></delete>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $trucks->links() }}
    <div class="mt-3"></div>
@endsection