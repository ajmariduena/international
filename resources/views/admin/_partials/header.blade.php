<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-minimizer d-md-down-none" type="button">☰</button>

    {{--  <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Dashboard</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Users</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="#">Settings</a>
        </li>
    </ul>  --}}
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown px-3">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" class="img-avatar">
                <span class="d-md-down-none">{{ Auth::user()->name }}</span>
            </a>
            <form class="dropdown-menu dropdown-menu-right m-2" method="POST" action="/logout">
                {{ csrf_field() }}
                <button type="submit" class="dropdown-item"><i class="fa fa-lock"></i> Cerrar sesión</button>
            </form>
        </li>
    </ul>
</header>