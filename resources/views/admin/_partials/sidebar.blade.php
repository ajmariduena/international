<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('users.index') }}"><i class="fa fa-user-circle"></i> Usuarios</a>
            </li>
            <li class="nav-title">
                Sitio web
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('pages.index') }}"><i class="fa fa-columns"></i> Páginas</a>
            <li class="nav-title">
                Productos
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('trucks.index') }}"><i class="fa fa-truck"></i> Camiones</a>
            <li class="nav-item nav-dropdown open">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-gears"></i> Repuestos</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('parts.filemanager') }}"><i class="fa fa-picture-o"></i> Imágenes</a>
                    </li>
                    {{-- <li class="nav-item open">
                        <a class="nav-link" href=""><i class="fa fa-upload"></i> Actualizar repuestos</a>
                    </li> --}}
                </ul>
            </li>
        </ul>
    </nav>
</div>