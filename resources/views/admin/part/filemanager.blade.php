@extends('admin.layouts.app')

@section('title', 'Imágenes')

@section('breadcrumb')
    {!! Breadcrumbs::render('images') !!}
@endsection

@section('content')
    <div class="filemanager-wrapper">
        <div id="filemanager1" class="filemanager"></div>  
    </div>
@endsection

@push('style')
    <link rel="stylesheet" href="{{ asset('css/filemanager.css') }}">
@endpush

@push('script')
    <script src="{{ asset('js/filemanager.js') }}"></script>
    <script>
        $(function() {
            $("#filemanager1").filemanager({
                url: '{{ route('imagenes.conex') }}',
                languaje: "ES",
                upload_max: 5,
                views:'thumbs',
                insertButton:true,
                token:'jashd4a5sd4sa'
            });
        });
    </script>
@endpush