@extends('admin.layouts.app')

@section('title', 'Crear Usuario')

@section('breadcrumb')
    {!! Breadcrumbs::render('create-user') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card btn-shadow">
                <div class="card-header">
                    <strong>Crear</strong> Usuario
                </div>
                <form method="POST" action="{{ route('users.store') }}">
                    {{ csrf_field() }}
                    <div class="card-block">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="title">Nombre</label>
                                <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" placeholder="Nombre de usuario">
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="title">Email</label>
                                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" id="email" name="email" placeholder="ejemplo@email.com">
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="title">Contraseña</label>
                                <input type="password" class="form-control @if($errors->has('password')) is-invalid @endif" id="password" name="password" placeholder="Mínimo 6 caracteres">
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-12">
                                <label for="roles">Roles</label>
                                <select class="form-control @if($errors->has('role')) is-invalid @endif" id="roles" name="role">
                                    <option selected disabled>Seleccionar un rol</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('role'))
                                    <div class="invalid-feedback">{{ $errors->first('role') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{ route('users.index') }}" class="btn btn-default">Volver</a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection