@extends('admin.layouts.app')

@section('title', 'Usuarios')

@section('breadcrumb')
    {!! Breadcrumbs::render('users') !!}
@endsection

@section('content')
    <div class="row justify-content-sm-between">
        <h3 class="m-0 font-weight-normal">Usuarios</h3>
        <a href="{{ route('users.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Nuevo</a>
    </div>
    <br>
    {{--  <div class="row">
        <div class="input-group btn-shadow">
            <input type="text" class="form-control btn-no-border" placeholder="Buscar usuarios..">
            <span class="input-group-btn">
                <button class="btn btn-primary btn-no-shadow" type="button"><i class="fa fa-search"></i> Buscar</button>
            </span>
        </div>
    </div>
    <br>  --}}
    <div class="row">
        <table class="table col btn-shadow">
            <thead>
                <tr>
                <th>#</th>
                <th>Nombres</th>
                <th>Correo</th>
                <th>Rol</th>
                <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->getRoleNames()->first() }}</td>
                        <td>
                            <a href="{{ route('users.edit', $user) }}" class="btn btn-primary btn-sm">Editar</a>
                            @if($users->total() > 1)
                                <delete route="{{ route('users.destroy', $user) }}" token="{{ csrf_token() }}"></delete>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $users->links() }}

@endsection