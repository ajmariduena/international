@extends('layout')

@section('title', $part->descripcion)

@section('content')

@php
    extract($components->all())
@endphp

@section('meta')
    <meta property="og:title" content="INTERNATIONAL | {{ $part->material }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $part->descripcion }}" />
@endsection

@include('_partials.modal-appointment')
	
  @isset($slider)
    <div class="tech-cover">
      <slider-component>
        @foreach($slider as $item)
          <div class="banner-item">
              <div class="container">
                <img class="page-banner-back" src="{{ optional($item->resources()->first())->url }}" alt="{{ $item->title }}">
              </div>
          </div>
        @endforeach
      </slider-component>
      <div class="caption">
        <h2>REPUESTOS</h2>
        <div>
        <a href="/repuestos/resultados">BUSCADOR REPUESTOS</a>
        <a href="{{ route('onsale')  }}">PROMOCIONES</a>
        </div>
      </div>
    </div>
  @endisset

	<div id="{{ $part->descripcion }}" style="margin-top: 24px;">
		<div class="container">
			<a class="back" href="{{ url()->previous() }}"><svg><use xlink:href="#back" /></svg>Regresar</a>
		</div>
	</div>
	
	<div class="results-wrapper">
		<div class="single-product-result">
			<div class="product">
				<div class="product-slider cycle-slideshow" 
					data-cycle-fx="scrollHorz"
					data-cycle-pager=".example-pager"
					data-cycle-timeout="4000">
					@forelse($part->images() as $image)
						<img src="/images/repuestos/{{ $image }}" alt="">
					@empty
						<img src="/images/default-thumbnail.png" alt="">
					@endforelse
				</div>
				<div class="product-description">
					<span class="name">{{ $part->grupo }}</span>
					<div class="price-group">
						<span class="price">Precio normal: <br>${{ $part->precio }}</span>
						<span class="price">Precio promoción: <br>${{ $part->precio_prom }}</span>
					</div>
					<p>
						Cód. Material: <br> {{ $part->material }}
					</p>
					<p>
						Descripción: <br>
						{{ $part->descripcion }}
					</p>
					<p>
						Stock en sucursal: <br>
						{{ $part->disponibilidad }}
					</p>
					<img src="/images/repuestos/repuestos-logos/{{$part->marca}}.png" class="logo">
					<form action="{{ route('addPartToCart') }}" method="POST" style="margin-top:1rem;">
						{{ csrf_field() }}
						<label for="" style="font-size:12px;text-transform:uppercase;display:block;margin-bottom:12px;">Cantidad:</label>
						<input type="number" min="1" value="1" name="quantity" style="width:75px;height:42px;font-size:16px;text-align:center;display:block;">
						<input type="text" hidden value="{{ $part->id }}" name="part">
						<button class="tech-btn" style="margin-top:12px;">Agregar al carrito</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	


@include('_partials.footer')

@endsection