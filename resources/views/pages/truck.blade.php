@extends('layout')

@section('title', $truck->name)

@section('meta')
    <meta property="og:title" content="INTERNATIONAL | {{ $truck->name }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $truck->description }}" />
@endsection

@section('content')
	
	@include('_partials.modal')

	@if(session('message'))
		<script>alert('{{ session('message') }}');</script>
	@endif
	
	<div class="single-product-banner">
		<img class="featured-image" src="{{ $truck->featured_image }}" alt="">
		<div class="container">
			<div class="name">
				<img src="{{ asset('images/international-iso.svg') }}" alt="">
				<div>
					<h1>{{ $truck->name }}</h1>
					<h2>{{ $truck->description }}</h2>
					<h2>
						USD ${{ $truck->price }}
						<span>*incluye I.V.A</span>
					</h2>
				</div>
				<hr>
			</div>
			<div class="options">
				<a href="" target="_blank">DESCARGAR CATÁLOGO</a>
				<a href="#" class="showModal">COTIZAR PRODUCTO</a>
			</div>
		</div>
	</div>

	<div class="top"></div>

	<div class="single-nav-wrapper">
	<nav class="single-product-nav">
		<li>
			<a @click.prevent="jump('.specs')">
				<div class="back-img">
					<img src="{{ asset('images/back-nav1.jpg') }}" alt="">
				</div>
				<span>CARACTERÍSTICAS TÉCNICAS</span>
			</a>
		</li>
		<li>
			<a @click.prevent="jump('.dimensions')">
			<div class="back-img">
				<img src="{{ asset('images/back-nav1.jpg') }}" alt="">
			</div>
			<span>CAPACIDAD Y DIMENSIONES</span>
			</a>
		</li>
		<li>
			<a @click.prevent="jump('.accessories')">
				<div class="back-img">
					<img src="{{ asset('images/back-nav1.jpg') }}" alt="">
				</div>
				<span>CABINA Y ACCESORIOS</span>
			</a>
		</li>
		<li>
			<a @click.prevent="jump('.gallery')">
				<div class="back-img">
					<img src="{{ asset('images/back-nav1.jpg') }}" alt="">
				</div>
				<span>GALERÍA DE FOTOS</span>
			</a>
		</li>
	</nav>
	</div>
	
	<div class="container">
		<div class="single-product specs">
			<div class="single-product-title">
				<h2>
					<span>CARACTERÍSTICAS</span>
					TÉCNICAS
				</h2>
			</div>
			<div class="single-product-content">
				<div>
					{!! $truck->specs !!}
				</div>
				@if(!empty($truck->specs_image))
					<div class="ml-75">
						<img src="{{ $truck->specs_image }}"/>
					</div>
				@endif
			</div>
		</div>
		<div class="single-product dimensions">
			<div class="single-product-title">
				<h2>
					<span>CAPACIDADES Y</span>
					DIMENSIONES
				</h2>
			</div>
			<div class="single-product-content">
				<div>
					{!! $truck->dimensions !!}
				</div>
				@if(!empty($truck->dimensions_image))
					<div class="ml-75">
						<img src="{{ $truck->dimensions_image }}"/>
					</div>
				@endif
			</div>
		</div>
		<div class="single-product 3">
			<div class="single-product-title">
				<h2>
					<span>CABINA Y</span>
					ACCESORIOS
				</h2>
			</div>
			<div class="single-product-content accessories">
				<div>
					{!! $truck->accessories !!}
				</div>
				@if(!empty($truck->accessories_image))
					<div class="ml-75">
						<img src="{{ $truck->accessories_image }}"/>
					</div>
				@endif
			</div>
		</div>
	</div>
	
	<div class="gallery">
		<div class="gallery-title">
			<div class="container">
				<div class="single-product-title">
					<h2>
						<span>GALERÍA</span>
						DE FOTOS
					</h2>
				</div>
			</div>
		</div>
		<div class="gallery-images">
			@foreach($truck->resources as $image)
				<a href="{{ $image->url }}" target="_blank"><img src="{{ $image->url }}" alt="{{ $truck->name }}"></a>
			@endforeach
		</div>
	</div>

  @include('_partials.footer')

@endsection