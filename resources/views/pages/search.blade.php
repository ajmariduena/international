@extends('layout')

@section('title', 'Buscador de Repuestos')

@php
    extract($components->all())
@endphp

@section('content')

  @include('_partials.modal-appointment')

	@isset($slider)
    <div class="tech-cover">
      <slider-component>
        @foreach($slider as $item)
          <div class="banner-item">
              <div class="container">
                <img class="page-banner-back" src="{{ optional($item->resources()->first())->url }}" alt="{{ $item->title }}">
              </div>
          </div>
        @endforeach
      </slider-component>
      <div class="caption">
        <h2>REPUESTOS</h2>
        <div>
        <a href="/repuestos/resultados">BUSCADOR REPUESTOS</a>
        <a href="{{ route('onsale') }}">PROMOCIONES</a>
        </div>
      </div>
    </div>
  @endisset

	<form class="filters" action="/repuestos/resultados" method="GET" class="form-to-submit">
		<label for="search" class="search">
			<input type="search" placeholder="Buscar por Descripción o Código de Material.." name="search" id="search">
			<svg><use xlink:href="#lens"></use></svg>
		</label>
		<div class="btn-group">
			<button class="tech-btn">Buscar</button>
			<a href="/repuestos/resultados" class="tech-btn">Limpiar filtros</a>
		</div>
		<div class="dropdowns">
			<label class="tippy" title="Marcas">
					<select name="marca" class="select-box">
						<option selected disabled>Marca</option>
						@foreach($brands as $brand)
							<option value="{{ $brand->name }}" @if(Request::get('marca') == $brand->name) selected @endif>{{ $brand->name }}</option>
						@endforeach
					</select>
					<svg><use xlink:href="#down" /></svg>
				</label>
				<label class="tippy" title="Grupos">
					<select name="grupo" class="select-box">
						<option selected disabled>Grupo</option>
						@foreach($groups as $group)
							<option value="{{ $group->name }}" @if(Request::get('grupo') == $group->name) selected @endif>{{ $group->name }}</option>
						@endforeach
					</select>
					<svg><use xlink:href="#down" /></svg>
				</label>
				<label class="tippy" title="Disponibilidad">
					<select name="disponibilidad" class="select-box">
						<option selected value="" disabled>Disponibilidad</option>
						<option value="g" @if(Request::get('disponibilidad') == 'g') selected @endif>Guayaquil</option>
						<option value="q" @if(Request::get('disponibilidad') == 'q') selected @endif>Quito</option>
						<option value="c" @if(Request::get('disponibilidad') == 'c') selected @endif>Cuenca</option>
					</select>
					<svg><use xlink:href="#down" /></svg>
				</label>
				<label class="tippy" title="Promoción">
					<select name="prom" class="select-box">
						<option selected disabled>Promoción</option>
						<option value="1" @if(Request::get('prom') == '1') selected @endif>En Promoción</option>
						<option value="0" @if(Request::get('prom') == '0') selected @endif>Sin promoción</option>
					</select>
					<svg><use xlink:href="#down" /></svg>
				</label>
		</div>
	</form>
	
	<div class="results-wrapper">
		<div class="results container">
			<table>
				@if(Request::all() > 1 && count($parts))
					<thead>
						<tr>	
							<th>Cód. Material</th>
							<th>Descripción</th>
							<th>Marca</th>
							<th>Grupo</th>
							<th>Precio normal</th>
							<th>Precio promoción</th>
						</tr>
					</thead>
				@endif
				<tbody>
					@forelse($parts as $part)
							<tr>
								<td><a href="{{ route('part', $part) }}#{{ $part->descripcion }}">{{ $part->descripcion }}</a></td>
								<td><a href="{{ route('part', $part) }}#{{ $part->descripcion }}">{{ ucfirst($part->descripcion) }}</a></td>
								<td>{{ $part->marca }}</td>
								<td>{{ $part->grupo }}</td>
								<td>${{ $part->precio }}</td>
								<td>
									@if($part->precio_prom !== '0.00')
										<span class="badge">${{ $part->precio_prom }}</span>
									@endif
								</td>
							</tr>
					@empty
							<tr>
								<td colspan=100% style="font-size:16px;font-weight:bold;padding-left: 0;">No se encontró ningún resultado</td>
							</tr>
					@endforelse
				</tbody>
			</table>
			@if(count($parts) > 0)
			{{ $parts->appends(
					[
						'marca' => Request::get('marca'),
						'grupo' => Request::get('grupo'),
						'dispinibilidad' => Request::get('dispinibilidad'),
						'prom' => Request::get('prom'),
					]
					)->links() }}
			@endif
		</div>
	</div>

	


@include('_partials.footer')

@endsection