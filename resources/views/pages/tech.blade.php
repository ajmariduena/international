@extends('layout')

@php
  extract($components->all())
@endphp

@section('title', $page->title)

@section('meta')
  <meta property="og:title" content="INTERNATIONAL | {{ $page->title }}" />
  <meta property="og:url" content="{{ url()->current() }}" />
  <meta property="og:description" content="{{ $page->description }}" />
@endsection

@section('content')

  @include('_partials.modal-tech')
	
  @isset($hero)
    <div class="tech-cover">
      <img src="{{ optional($hero->first()->resources()->first())->url }}" alt="">
    </div>
  @endisset

  @if(session('message'))
    <script>alert('{{ session('message') }}');</script>
  @endif

  <div class="container about-padding">
    @isset($servicio_tecnico)
      <div class="about-grid" style="padding: 44px 0;">
        <div class="tech-title tech-title-center">
          <div class="single-product-title">
            <h2>
              <span>{{ $servicio_tecnico->first()->title }}</span>
              <hr class="tech-line">
            </h2>
            <p>{{ $servicio_tecnico->first()->subtitle }}</p>
          </div>
        </div>
        <div class="tech-content">
          {!! $servicio_tecnico->first()->content !!}
          <a href="" target="_blank" class="tech-btn">DESCARGAR DOCUMENTO</a>
          <a href="" class="showModal tech-btn">SOLICITAR CITA</a>
        </div>
      </div>
    @endisset
  </div>

  @include('_partials.footer')

@endsection