@extends('layout')

@php
    extract($components->all())
@endphp

@section('title', $page->title)

@section('meta')
    <meta property="og:title" content="INTERNATIONAL | {{ $page->title }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $page->description }}" />
@endsection

@section('content')
	
  @isset($hero)
  <div class="container about-cover">
    <img src="{{ optional($hero->first()->resources()->first())->url }}" alt="{{ $hero->first()->title }}">
  </div>
  @endisset

  <div class="container about-padding">
    @isset($international_en_el_mundo)
    <div class="single-product-title">
      <h2>
        {{ $international_en_el_mundo->first()->title }}
      </h2>
    </div>
    @endisset
    <div class="about-grid">
      @isset($international_en_el_mundo)
        <div>
          {!! $international_en_el_mundo->first()->content !!}
        </div>
      @endisset
      @isset($nuestra_historia)
        <div>
          {!! $nuestra_historia->first()->content !!}
        </div>
      @endisset
    </div>
    {{--  <div class="about-grid">
      <div>
    <a style="color: #f0802c; font-weight: bold;" href="{{ asset('/resolucion-cancelacion-motransa-2015.pdf') }}" target="_blank">Cancelación de papel comercial</a>
    <a style="color: #f0802c; font-weight: bold;" href="{{ asset('/ResolucionCancelaciondeEmisordeValores.pdf') }}" target="_blank">Resolución de Cancelación de Emisor de Valores</a>
      </div>  --}}
    </div>
  </div>

  <div class="about-featured">
    <div class="triangle-top"></div>
    <div class="container">
      @isset($international_en_el_ecuador)
        <div class="single-product-title">
          <h2>
            {{ $international_en_el_ecuador->first()->title }}
          </h2>
        </div>
        <div class="about-grid">
          <div>
            {!! $international_en_el_ecuador->first()->content !!}
          </div>
          <div>
            <img src="{{ optional($international_en_el_ecuador->first()->resources()->first())->url }}">
          </div>
        </div>
      @endisset
      <div class="about-grid">
        <div>
          @isset($mision)
            <div class="single-product-title">
              <h2>
              {{ $mision->first()->title }}
              </h2>
            </div>
            <br>
            {!! $mision->first()->content !!}
            <br>
          @endisset
          @isset($vision)
            <div class="single-product-title">
              <h2>
              {{ $vision->first()->title }}
              </h2>
            </div>
            <br>
            {!! $vision->first()->content !!}
          @endisset
        </div>
        @isset($valores)
          <div>
            <div class="single-product-title">
              <h2>
              {{ $valores->first()->title }}
              </h2>
            </div>
            {!! $valores->first()->content !!}
          </div>
        @endisset
      </div>
      <div class="triangle-bottom"></div>
    </div>
  </div>

  <div class="container about-padding" id="featured">
    <div>
      <div class="single-product-title">
        <h2>
          <span>INTERNATIONAL</span>
          EN EL MUNDO
        </h2>
      </div>
    </div>
    <div class="about-grid">
      @isset($mapa)
        <div>
          {!! $mapa->first()->content !!}
        </div>
      @endisset
      @isset($direcciones)
      <div>
        {!! $direcciones->first()->content !!}
      </div>
      @endisset
    </div>
  </div>

  @isset($trabaja_con_nosotros)
  <div class="work-with-us" style="background: url('{{ optional($trabaja_con_nosotros->first()->resources()->first())->url }}')">
    <div class="filter"></div>
    <div class="container">
      <div class="about-grid">
        <div>
          <h1>{{ $trabaja_con_nosotros->first()->title }}</h1>
          {!! $trabaja_con_nosotros->first()->content !!}
        </div>
        <div>
          <a href="http://mitsubishi.bumeran.com.ec/login.bum" target="_blank" class="tech-btn">APLICAR</a>
        </div>
      </div>
    </div>
  </div>
  @endisset

  @include('_partials.footer')

@endsection