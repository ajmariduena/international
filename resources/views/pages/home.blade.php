@extends('layout')

@php
    extract($components->all())
@endphp

@section('title', $page->title)

@section('meta')
    <meta property="og:title" content="INTERNATIONAL | {{ $page->title }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $page->description }}" />
@endsection


@section('content')

	@isset($slider)
		<fixed-slider>	
			@foreach($slider as $item)
				<div class="slider-item">
					<img src="{{ optional($item->resources()->first())->url }}" alt="">
					<div class="darker-filter"></div>
					<div class="slider-content">
						<h1 class="slider-title">{{ $item->title }}</h1>
						<hr class="slider-line">
					</div>
				</div>
			@endforeach
		</fixed-slider>
	@endisset

	<footer class="footer fixed-footer">
        <div class="container">
            <p class="copyright"><b>© 2016 MOTRANSA.</b> TODOS LOS DERECHOS RESERVADOS | <a href="http://www.geniale.ec/" title="Desarrollo web" target="_blank"><b>Geniale</b></a></p>
            <ul class="footer-icons">
				<li><a href="https://www.facebook.com/internationaltrucks/?fref=ts" target="_blank"><svg><use xlink:href="#facebook" /></svg></a></li>
				<li><a href="https://twitter.com/intnltrucks" target="_blank"><svg><use xlink:href="#twitter" /></svg></a></li>
				<li><a href="https://www.youtube.com/user/Internationaltruck" target="_blank"><svg><use xlink:href="#youtube" /></svg></a></li>
            </ul>
        </div>
    </footer>

@endsection