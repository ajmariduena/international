@extends('layout')

@php
    extract($components->all())
@endphp

@section('title', $page->title)

@section('meta')
    <meta property="og:title" content="INTERNATIONAL | {{ $page->title }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $page->description }}" />
@endsection

@section('content')  
  @isset($slider)
    <div class="tech-cover">
      <slider-component>
        @foreach($slider as $item)
          <div class="banner-item">
              <div class="container">
                <img class="page-banner-back" src="{{ optional($item->resources()->first())->url }}" alt="{{ $item->title }}">
              </div>
          </div>
        @endforeach
      </slider-component>
      <div class="caption">
        <h2>REPUESTOS</h2>
        <div>
        <a href="/repuestos/resultados">BUSCADOR REPUESTOS</a>
        <a href="{{ route('onsale')  }}">PROMOCIONES</a>
        </div>
      </div>
    </div>
  @endisset

	@if(session('message'))
		<script>alert('{{ session('message') }}');</script>
	@endif

	<div class="container">
		<ul class="parts-list">
			<li>
				<a href="{{ route('search', ['grupo' => 'Lubricantes']) }}">
				<svg><use xlink:href="#lubri" /></svg>
				<span>LUBRICANTES</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Neumaticos']) }}">
				<svg><use xlink:href="#neu" /></svg>
				<span>NEUMÁTICOS</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Motor']) }}">
				<svg><use xlink:href="#motor" /></svg>
				<span>REPUESTOS MOTOR</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Transmisión']) }}">
				<svg><use xlink:href="#trans" /></svg>
				<span>REPUESTOS TRANSMISIÓN</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Suspensión']) }}">
				<svg><use xlink:href="#suspen" /></svg>
				<span>REPUESTOS SUSPENSIÓN</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Frenos']) }}">
				<svg><use xlink:href="#frenos" /></svg>
				<span>REPUESTOS FRENOS</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Enfriamiento']) }}">
				<svg><use xlink:href="#enfri" /></svg>
				<span>REPUESTOS ENFRIAMIENTO</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Carroceria']) }}">
				<svg><use xlink:href="#carro" /></svg>
				<span>REPUESTOS CARROCERÍA</span>
				</a>
			</li>
			<!-- line -->
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Interiores']) }}">
				<svg><use xlink:href="#inte" /></svg>
				<span>REPUESTOS INTERIORES</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Dirección']) }}">
				<svg><use xlink:href="#dire" /></svg>
				<span>REPUESTOS DIRECCIÓN</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Filtración']) }}">
				<svg><use xlink:href="#filtra" /></svg>
				<span>REPUESTOS FILTRACIÓN</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Elétricos']) }}">
				<svg><use xlink:href="#elec" /></svg>
				<span>REPUESTOS ELÉCTRICOS</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos Accesorios']) }}">
					<svg><use xlink:href="#acce" /></svg>
					<span>REPUESTOS ACCESORIOS</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search', ['grupo' => 'Repuestos HVAC']) }}">
					<svg><use xlink:href="#hvac" /></svg>
					<span>REPUESTOS HVAC</span>
				</a>
			</li>
			<li>
				<a href="{{ route('search') }}">
					<svg><use xlink:href="#search" /></svg>
					<span>BUSCADOR REPUESTOS</span>
				</a>
			</li>
		</ul>
	</div>

	@include('_partials.footer')
@endsection