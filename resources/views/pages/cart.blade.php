@extends('layout')

@section('title', 'Carrito')

@push('style')
  <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
@endpush

@section('content')

  <div class="container" style="margin-top: 85px;">
    <h1 class="text-lg block text-center text-orange font-bold">CARRITO DE PEDIDOS</h1>
    <form  action="{{ route('cart.update') }}" method="POST" class="max-w-md mx-auto">
      {{ csrf_field() }}
      @if(count($cart->content()) > 0)
        <div class="px-4 my-4">
          <a class="back" href="{{ url()->previous() }}"><svg><use xlink:href="#back" /></svg>Regresar</a>
        </div>
        <div class="px-4">
            @foreach($cart->content() as $item)
              <div class="flex flex-column md:flex-row flex-wrap border-b border-grey-light border-solid mb-3"> 
                <div class="pl-0 w-full hidden md:block" style="max-width:200px;">
                  <img src="/images/default-thumbnail.png" alt="" class="w-full h-100 object-contain">
                </div>
                <input type="text" value="{{ $item->rowId }}" hidden name="cart[{{ $loop->index }}][rowId]">
                <div class="my-3 w-full md:flex-1">
                  <div>
                    <span class="text-lg font-bold">{{ $item->name }}</span>        
                  </div>
                  <div class="flex">
                    <div class="flex-1 md:max-w-50">
                      <input type="number" value="{{ $item->qty }}" min="1" class="my-3 border border-black w-12 h-12 text-center md:h-10" name="cart[{{ $loop->index }}][qty]">     
                    </div>
                    <div class="flex py-3 flex-1 md:ml-4">
                      <div class="w-1/2 flex items-center">
                        <span class="md:text-lg">${{ $item->price }}</span>
                      </div>
                      <div class="w-1/2 flex items-center justify-end">
                          <a href="{{ route('cart.delete', $item->rowId) }}"><svg class="w-6 h-6 text-red"><use xlink:href="#trash" /></svg></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
        </div>
        <hr>
        <div class="p-4">
          <div class="mb-3">
            <div class="mb-3 flex justify-between">
              <span>SUBTOTAL</span><span>${{ $cart->subtotal }}</span>
            </div>
            <div class="mb-3 flex justify-between">
              <span>I.V.A.</span><span>${{ $cart->tax() }}</span>
            </div>
            <div class="flex justify-between bg-grey p-2">
              <span>TOTAL</span><span>${{ $cart->total() }}</span>
            </div>
          </div>
          <div class="flex justify-end">
            <button type="submit" href="#" class="bg-transparent border border-solid border-black text-black tech-btn px-6 text-lg mr-3 mt-0" style="color:black !important;">Acualizar carrito</button>
            <a href="{{ route('cart.order') }}" class="tech-btn text-lg px-6 mt-0">Cotizar</a>
          </div>
        </div>
      @else
        <h5 class="px-4 mt-6">No hay ningún producto agregado.</h5>
      @endif
    </form>
  </div>

  @include('_partials.footer')
@endsection