@extends('layout')

@php
    extract($components->all())
@endphp

@section('title', $page->title)

@section('meta')
    <meta property="og:title" content="INTERNATIONAL | {{ $page->title }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $page->description }}" />
@endsection

@section('content')
  @isset($slider)
    <slider-component>
      @foreach($slider as $item)
        <div class="banner-item">
            <div class="container">
              <img class="page-banner-back" src="{{ optional($item->resources()->first())->url }}" alt="{{ $item->title }}">
              <div class="page-banner-content">
                <div class="page-banner-logo">
                  <img src="{{ asset('images/international-iso.svg') }}" alt="International">
                </div>
                <h1>{{ $item->title }}</h1>
              </div>
            </div>
        </div>
      @endforeach
    </slider-component>
  @endisset

  @include('composers.trucks')

  @include('_partials.footer')
@endsection