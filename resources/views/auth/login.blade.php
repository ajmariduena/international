<!doctype html>
<html class="no-js" lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        @yield('meta')
        <title>INTERNATIONAL | @yield('title') | Motransa S.A. Ecuador</title>
        <link rel="icon" type="image/png" href="{{ asset('/favicon.png') }}" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!--Start of Zendesk Chat Script-->
        <script type="text/javascript">
          window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
          d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
          _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
          $.src="https://v2.zopim.com/?56xUNtuHRdzBtCexNh7FgOR0Ch1ohsNk";z.t=+new Date;$.
          type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
          $zopim(function() {
              $zopim.livechat.button.setOffsetVertical(0);
              $zopim.livechat.window.setSize('medium');
              $zopim.livechat.window.setTitle('Motransa');
          });
        </script>
        <!--End of Zendesk Chat Script-->
    </head>
    <body style="margin-top: 44px;">
        @include('_partials.svg')
        <div id="app">
            <div class="background-login"><img src="/images/background-login.png"></div>
            <div class="login-wrapper">
                <a href="/" class="login-logo">
                    <img src="/images/international-logo.svg" alt="">
                </a>
                <div class="login-form">
                    <h1>INICIAR SESIÓN</h1>
                    <form action="/login" method="POST">
                        <label for="">Correo electrónico</label>
                        <input type="email" name="email">
                        @if($errors->has('email'))
                            <div class="error">{{ $errors->first('email') }}</div>
                        @endif
                        <label for="" style="margin-top:16px;">Contraseña</label>
                        <input type="password" style="margin-bottom:0;" name="password">
                        @if($errors->has('password'))
                            <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                        <button class="tech-btn">Iniciar sesión</button>
                        <a href="">¿OLVIDASTE TU CONTRASEÑA?</a>
                    </form>
                    <hr>
                    <div>
                        <a href="{{ url()->previous() }}"><svg><use xlink:href="#back" /></svg>Volver</a>
                        <a href="/register">Crear Cuenta</a>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>