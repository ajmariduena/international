@extends('layout')

@section('title', 'Carrito')

@push('style')
  <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
@endpush

@section('content')

  <div class="container" style="display:flex;height:100vh;width:100vw;align-items:center;justify-content:center;">
    <div>
      <h1 class="text-2xl block text-center text-orange font-bold">COTIZACIÓN GENERADA EXISTOSAMENTE</h1>
      <p class="leading-loose my-4">Hemos enviado la cotización a su correo {{ Auth::user()->email }}.</p>
      <a href="/" class="tech-btn mt-0 mx-auto text-lg">Inicio</a>
    </div>
  </div>

  @include('_partials.footer')
@endsection