<div class="container">
    @foreach($families as $family)
    <div class="product-wrapper">
      <h2 class="product-category">{{ $family->name }}</h2>
      <div class="product-grid">
        @foreach($family->trucks as $truck)
        <div class="product-item">
          <a href="{{ route('truck', $truck) }}" class="product-image">
            <img src="{{ $truck->featured_image }}" alt="{{ $truck->nombre }}">
          </a>
          <div class="product-info">
            <span class="product-name">{{ $truck->name }}</span>
            <hr>
            <ul class="product-features">
              <li>{{ $truck->description }}</li>
              <li>Capacidad de carga: {{ $truck->load_amount }}</li>
            </ul>
            <a href="{{ route('truck', $truck) }}" class="product-details">Detalles del vehículo</a>
          </div>
        </div>
        @endforeach
      </div>
    </div>
    @endforeach
  </div>