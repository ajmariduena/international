@if($cart->count() > 0)
    <div class="cart-button-wrapper">
        <div class="container">
            <a href="{{ route('cart') }}" class="cart-button tippy" title="{{ $cart->count() }} productos" data-amount="{{ $cart->count() }}">
                <svg><use xlink:href="#cart" /></svg>
            </a>
        </div>
    </div>
@endif
