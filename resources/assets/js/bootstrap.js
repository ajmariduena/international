
import lodash from 'lodash';
import jquery from 'jquery';
import vue from 'vue';
import axios from 'axios';

window._ = lodash;
window.$ = jquery;
window.axios = axios;
window.Vue = vue;


window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
