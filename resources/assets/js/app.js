require('./bootstrap');

import objectFitImages from 'object-fit-images';
import tippy from 'tippy.js';
import slick from 'slick-carousel/slick/slick.min';
import jump from 'jump.js'

objectFitImages();

/* Components */
import FixedSlider from './components/FixedSlider.vue';
import Slider from './components/Slider.vue';

Vue.component('fixed-slider', FixedSlider);
Vue.component('slider-component', Slider);

new Vue({
    el: '#app',
    data() {
        return {
            menu: false
        }
    },
    mounted() {
        /* Run tippy.js on mounted */
        tippy('.tippy', { animation: 'fade', arrow: true });
    },
    methods: {
        jump($target) {
            jump($target, {
                offset: -50,
            });
        }
    }
});
