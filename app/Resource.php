<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = ['url', 'mime', 'size','attributes'];
    
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getUrlAttribute($value)
    {
        return asset("storage/{$value}");
    }
}
