<?php

namespace App\Traits;

trait ModelTrait {

  public function getModelNameAttribute()
  {
      return class_basename($this);
  }

  public function getPluralNameAttribute()
  {
    return \strtolower(\str_plural(class_basename($this)));
  }
}