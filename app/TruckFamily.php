<?php

namespace App;

use App\Truck;
use Illuminate\Database\Eloquent\Model;

class TruckFamily extends Model
{
    public function trucks()
    {
        return $this->hasMany(Truck::class);
    }
}
