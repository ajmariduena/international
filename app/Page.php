<?php

namespace App;

use App\Component;
use App\Traits\ModelTrait;
use App\CreatableComponent;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use ModelTrait;
    protected $fillable = ['title', 'description', 'slug', 'view'];
    protected $with = ['components', 'creatableComponents'];
    protected $appends = ['model_name'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function components()
    {
        return $this->morphMany(Component::class, 'componentable');
    }

    public function creatableComponents()
    {
        return $this->morphMany(CreatableComponent::class, 'creatable');
    }
    
    public function getActiveComponents()
    {
        return $this->components()->where('status', true);
    }
}
