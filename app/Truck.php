<?php

namespace App;

use App\TruckFamily;
use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    use ModelTrait;
    protected $fillable = ['name', 'description', 'load_amount', 'price', 'specs', 'specs_image', 'dimensions', 'dimensions_image', 'accessories', 'accessories_image', 'featured_image', 'catalog', 'status'];

    public function family()
    {
        return $this->belongsTo(TruckFamily::class, 'truck_family_id');
    }

    public function resources()
    {
        return $this->morphMany(Resource::class, 'imageable');
    }

    public function getSpecsImageAttribute($value)
    {
        if($value) {
            return asset("storage/{$value}");
        }
    }

    public function getDimensionsImageAttribute($value)
    {
        if($value) {
            return asset("storage/{$value}");
        }
    }

    public function getFeaturedImageAttribute($value)
    {
        if($value) {
            return asset("storage/{$value}");
        }
    }

    public function getAccessoriesImageAttribute($value)
    {
        if($value) {
            return asset("storage/{$value}");
        }
    }

    public function getCatalogAttribute($value)
    {
        if($value) {
            return asset("storage/{$value}");
        }
    }
}
