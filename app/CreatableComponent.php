<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreatableComponent extends Model
{
    protected $table = 'creatable_component';
}
