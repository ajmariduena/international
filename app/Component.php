<?php

namespace App;

use App\Resource;
use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use ModelTrait;
    protected $fillable = ['key', 'title', 'subtitle', 'content', 'attributes','status', 'componentable_id', 'componentable_type'];
    protected $with = ['resources'];
    protected $appends = ['model_name', 'plural_name'];

    public function resources()
    {
        return $this->morphMany(Resource::class, 'imageable');
    }

    public function componentable()
    {
        return $this->morphTo();
    }

    public function getTypeOfAttribute()
    {
        return str_plural(strtolower(class_basename($this->componentable)));
    }
}
