<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class Part extends Model implements Buyable
{
    protected $fillable = ['material', 'descripcion', 'marca', 'grupo', 'prom', 'disp_q', 'disp_c', 'disp_g', 'precio', 'precio_prom', 'unidad'];

    public function images()
    {
        $name = \strtolower($this->material);
        $files = preg_grep("~^{$name}.*~", scandir(\public_path() . "/images/repuestos"));
        return $files;
    }

    public function getRouteKeyName()
    {
        return 'material';
    }

    public function getDescripcionAttribute($value)
    {
        return ucfirst(\strtolower($value));
    }

    public function getDisponibilidadAttribute()
    {
        if ($this->disp_g) {
            $string['gye'] = ' Guayaquil';
        }
        if ($this->disp_q) {
            $string['quito'] = ' Quito';
        }
        if ($this->disp_c) {
            $string['cuenca'] = ' Cuenca';
        }
        return implode(',', $string);
    }

    /* Cart specified */
    public function getBuyableIdentifier($options = null)
    {
        return $this->material;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->descripcion;
    }

    public function getBuyablePrice($options = null)
    {
        if ($this->precio_prom > 0) {
            return $this->precio_prom;
        } else {
            return $this->precio;
        }
    }
}
