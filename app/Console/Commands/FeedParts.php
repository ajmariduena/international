<?php

namespace App\Console\Commands;

use App\Part;
use Illuminate\Console\Command;

class FeedParts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repuestos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analiza el archivo de texto plano y obtiene todos los repuestos existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = file(\base_path(env("ARCHIVO_PLANO", "motransa.txt")));
        $items = [];
        $keys = ['material', 'descripcion', 'marca', 'grupo', 'prom', 'disp_q', 'disp_c', 'disp_g', 'precio', 'precio_prom', 'unidad'];
        try {
            foreach ($file as $line) {
                $line = explode("\\", $line);
                $line = array_map(
                    function ($a) {
                        return \str_replace('            ', '', $a);
                    },
                    $line
                );
                if (count($line) === 11) {
                    $line = array_combine($keys, $line);
                    \array_push($items, $line);
                }
            }
            foreach ($items as $item) {
                Part::updateOrCreate([
                    'material' => $item['material']
                ], $item);
            }
            $this->info('Los Repuestos se han actualizado correctamente');
        } catch (\Exception $e) {
            $this->error('Algo ha salido mal con la actualización de repuestos.');
            $this->error($e);
        }
    }
}
