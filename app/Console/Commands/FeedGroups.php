<?php

namespace App\Console\Commands;

use App\Group;
use Illuminate\Console\Command;

class FeedGroups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repuestos:grupos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analiza el archivo de texto plano y obtiene todos los grupos existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = file(\base_path(env("ARCHIVO_PLANO", "motransa.txt")));
        $groups = [];
        foreach ($file as $line) {
            $group = explode("\\", $line);
            if (isset($group[3])) {
                array_push($groups, $group[3]);
            }
        }
        $groups = array_unique($groups);
        foreach ($groups as $group) {
            Group::firstOrCreate(['name' => $group]);
        }
        $this->info('Los Grupos se han actualizado correctamente');
    }
}
