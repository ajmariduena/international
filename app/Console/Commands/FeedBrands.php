<?php

namespace App\Console\Commands;

use App\Brand;
use Illuminate\Console\Command;

class FeedBrands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repuestos:marcas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Analiza el archivo de texto plano y obtiene todas las marcas existentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = file(\base_path(env("ARCHIVO_PLANO", "motransa.txt")));
        $brands = [];
        foreach ($file as $line) {
            $brand = explode("\\", $line);
            if (isset($brand[2])) {
                array_push($brands, $brand[2]);
            }
        }
        $brands = array_unique($brands);
        foreach ($brands as $brand) {
            Brand::firstOrCreate(['name' => $brand]);
        }
        $this->info('Las Marcas se han actualizado correctamente');
    }
}
