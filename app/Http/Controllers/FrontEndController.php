<?php

namespace App\Http\Controllers;

use App\Page;
use App\Part;
use App\Truck;
use App\Brand;
use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontEndController extends Controller
{
    public function home()
    {
        $page = Page::first();
        $components = $page->getActiveComponents()->get()->groupBy('key');
        return view("pages.{$page->view}", compact('page', 'components'));
    }

    public function page(Page $page)
    {
        $components = $page->getActiveComponents()->get()->groupBy('key');
        return view("pages.{$page->view}", compact('page', 'components'));
    }

    public function truck(Truck $truck)
    {
        return view('pages.truck', \compact('truck'));
    }

    public function search(Request $request)
    {
        $page = Page::where('id', 4)->first();
        $components = $page->getActiveComponents()->get()->groupBy('key');

        $brands = Brand::all();
        $groups = Group::all();

        $search = $request->search;
        $marca = $request->has('marca') ? $request->marca : false;
        $grupo = $request->has('grupo') ? $request->grupo : false;
        $disponibilidad = $request->has('disponibilidad') ? $request->disponibilidad : false;
        $prom = $request->has('prom') ? $request->prom : false;

        if (count($request->all()) > 0) {
            $parts = Part::when($grupo, function ($query) use ($grupo) {
                return $query->where('grupo', $grupo);
            })->when($search, function ($query) use ($search) {
                return $query->where(function ($query) use ($search) {
                    return $query->orWhere('material', 'LIKE', '%' . $search . '%')
                        ->orWhere('descripcion', 'LIKE', '%' . $search . '%');
                });
            })
                ->when($marca, function ($query) use ($marca) {
                    return $query->where('marca', $marca);
                })
                ->when($disponibilidad, function ($query) use ($disponibilidad) {
                    if ($disponibilidad == 'g') {
                        return $query->where('disp_g', true);
                    } elseif ($disponibilidad == 'q') {
                        return $query->where('disp_q', true);
                    } elseif ($disponibilidad == 'c') {
                        return $query->where('disp_c', true);
                    }
                })
                ->when('prom', function ($query) use ($prom) {
                    return $query->where('prom', $prom);
                })
                ->where(function ($query) {
                    $query->orWhere('disp_g', '!=', false)
                    ->orWhere('disp_q', '!=', false)
                    ->orWhere('disp_c', '!=', false);
                })
                ->orderBy('material')
                ->simplePaginate(25);
        } else {
            $parts= [];
        }
        return view('pages.search', compact('parts', 'brands', 'groups', 'page', 'components'));
    }

    public function part(Part $part)
    {
        $page = Page::where('id', 4)->first();
        $components = $page->getActiveComponents()->get()->groupBy('key');
        return view('pages.part', \compact('part', 'page', 'components'));
    }

    public function onsale()
    {
        $page = Page::where('id', 4)->first();
        $components = $page->getActiveComponents()->get()->groupBy('key');
        
        $parts = Part::where('prom', true)
        ->where(function ($query) {
            $query->orWhere('disp_g', '!=', false)
            ->orWhere('disp_q', '!=', false)
            ->orWhere('disp_c', '!=', false);
        })
        ->paginate(5);
        return view('pages.onsale', \compact('parts', 'page', 'components'));
    }

    public function mail(Request $request)
    {
        $messages = [
            'telefono.required'=>'Teléfono requerido.',
            'email.required'=>'Correo eletrónico requerido.',
            'email.email'=>'Correo eletrónico inválido.',
            'message.required'=>'Mensaje requerido'
        ];
        $this->validate($request, [
            'phone' => 'required',
            'email'=> 'email|required',
            'message' => 'required'
        ], $messages);
        Mail::to('ventas@salcedointernacional.com')->send(new ContactRequest($request->all()));
        return redirect()->back()->with('message', 'message');
    }
}
