<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuillermoMartinez\Filemanager\Filemanager;
use Illuminate\Support\Facades\Artisan;

class PartsController extends Controller
{
    public function filemanager()
    {
        return view('admin.part.filemanager');
    }

    public function getConnection()
    {
        $extra = array(
            "source" => "images/repuestos",
            "url" => "/",
            );
        $f = new Filemanager($extra);
        $f->run();
    }

    public function postConnection(Request $request)
    {
        $extra = array(
            "source" => "images/repuestos",
            "url" => "/",
            );
        if ($request->has('typeFile') && $request->input('typeFile') == 'images') {
            $extra['type_file'] = 'images';
        }
        $f = new Filemanager($extra);
        $f->run();
    }
}
