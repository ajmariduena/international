<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $pages = Page::paginate(15);
        return view('admin.page.index', compact('pages'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        /* Get all blade files in resources/pages */
        $views = array_diff(scandir(base_path('/resources/views/pages')), array('..', '.'));
        $views = array_map(function($view) {
            return \explode(".", $view)[0];
        }, $views);
        return view('admin.page.create', \compact('views'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:pages',
            'view'  => 'required'
        ]);
        $data = $request->all();
        $data['slug'] = \str_slug($request->title);
        Page::create($data);
        return redirect()->route('pages.index');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Page $page)
    {
        return view('admin.page.edit', \compact('page'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
        $page->update($request->only(['title', 'description']));
        return redirect()->route('pages.index');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Page $page)
    {
        $page->delete();
        return redirect()->route('pages.index');
    }
}
