<?php

namespace App\Http\Controllers\Admin;

use App\Resource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResourcesController extends Controller
{
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        /* Get the Model Name on the request */
        $model = "App\\{$request->model}";
        $model = $model::find($request->id);
        foreach ($request->file as $value) {
            $model->resources()->save(new Resource([ 
                'url' => $value->store('resources'),
                'mime' => \Illuminate\Support\Facades\File::mimeType($value),
                'size' => \Illuminate\Support\Facades\File::size($value)
            ]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $resource)
    {
        $resource->delete();
    }
}
