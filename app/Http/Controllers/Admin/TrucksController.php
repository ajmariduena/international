<?php

namespace App\Http\Controllers\Admin;

use App\Truck;
use App\TruckFamily;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrucksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trucks = Truck::with('family')->orderBy('truck_family_id')->paginate(25);
        return view('admin.truck.index', \compact('trucks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $families = TruckFamily::all();
        return view('admin.truck.create', \compact('families'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'load_amount' => 'required',
            'price' => 'required',
            'featured_image' => 'required|mimes:jpeg,bmp,png',
            'truck_family_id' => 'required',
            'catalog' => 'sometimes|nullable|mimes:pdf',
            'specs_image' => 'sometimes|nullable|mimes:jpeg,bmp,png',
            'dimensions_image' => 'sometimes|nullable|mimes:jpeg,bmp,png',
            'accessories_image' => 'sometimes|nullable|mimes:jpeg,bmp,png',            
        ]);
        $data = $request->except(['truck_family_id', 'specs_image', 'featured_image', 'dimensions_image', 'accessories_image', 'catalog']);
        /* Store Catalog pdf */
        if($request->hasFile('catalog')) {
            $data['catalog'] = $request->file('catalog')->store('resources');
        }
        /* Store Featured image */
        if($request->hasFile('featured_image')) {
            $data['featured_image'] = $request->file('featured_image')->store('resources');
        }
        /* Store Tech specs image */
        if($request->hasFile('specs_image')) {
            $data['specs_image'] = $request->file('specs_image')->store('resources');
        }
        /* Store Dimensaions image */
        if($request->hasFile('dimensions_image')) {
            $data['dimensions_image'] = $request->file('dimensions_image')->store('resources');
        }
        /* Store Accessories image */
        if($request->hasFile('accessories_image')) {
            $data['accessories_image'] = $request->file('accessories_image')->store('resources');
        }
        $family = TruckFamily::where('id', $request->input('truck_family_id'))->first();
        $family->trucks()->save(new Truck($data));
        return redirect()->route('trucks.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Truck $truck)
    {
        $families = TruckFamily::all();
        return view('admin.truck.edit', \compact('truck', 'families'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Truck $truck)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'load_amount' => 'required',
            'price' => 'required',
            'featured_image' => 'sometimes|nullable|mimes:jpeg,bmp,png',
            'truck_family_id' => 'required',
            'catalog' => 'sometimes|nullable|mimes:pdf'
        ]);
        $family = TruckFamily::where('id', $request->input('truck_family_id'))->first();
        $data = $request->except(['truck_family_id', 'specs_image', 'featured_image', 'dimensions_image', 'accessories_image', 'catalog']);
        $data['status'] = $request->has('status') ? 1 : 0;
        /* Store Catalog pdf */
        if($request->hasFile('catalog')) {
            $data['catalog'] = $request->file('catalog')->store('resources');
        }
        /* Store Featured image */
        if($request->hasFile('featured_image')) {
            $data['featured_image'] = $request->file('featured_image')->store('resources');
        }
        /* Store Tech specs image */
        if($request->hasFile('specs_image')) {
            $data['specs_image'] = $request->file('specs_image')->store('resources');
        }
        /* Store Dimensaions image */
        if($request->hasFile('dimensions_image')) {
            $data['dimensions_image'] = $request->file('dimensions_image')->store('resources');
        }
        /* Store Accessories image */
        if($request->hasFile('accessories_image')) {
            $data['accessories_image'] = $request->file('accessories_image')->store('resources');
        }
        $truck->update($data);
        $truck->family()->associate($family);
        $truck->save();
        return redirect()->route('trucks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Truck $truck)
    {
        $truck->delete();
        return redirect()->route('trucks.index');
    }
}
