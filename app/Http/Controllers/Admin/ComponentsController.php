<?php

namespace App\Http\Controllers\Admin;

use App\Resource;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentsController extends Controller
{
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create($type, $id)
    {
        $name = "App\\{$type}";
        $source = $name::find($id);
        return view('admin.component.create', \compact('source'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'key' => 'required'
        ]);
        $data = $request->all();
        $component = new Component($data);
        $component->save();
        if($request->hasFile('resource')) {
            $file = $request->file('resource');
            $component->resources()->save(new Resource([ 
                'url' => $file->store('resources'),
                'mime' => \Illuminate\Support\Facades\File::mimeType($file),
                'size' => \Illuminate\Support\Facades\File::size($file)
            ]));
        }
        return redirect()->route("{$component->componentable->plural_name}.edit", $component->componentable);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit(Component $component)
    {
        return view('admin.component.edit', \compact('component'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Component $component)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $data = $request->all();
        $data['status'] = $request->has('status') ? 1 : 0;
        $component->update($data);
        return redirect()->route("{$component->componentable->plural_name}.edit", $component->componentable);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Component $component)
    {
        $component->delete();
        return redirect()->route("{$component->componentable->plural_name}.edit", $component->componentable);
    }
}
