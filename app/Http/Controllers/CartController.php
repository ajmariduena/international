<?php

namespace App\Http\Controllers;

use App\Part;
use Carbon\Carbon;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function cart()
    {
        return view('pages.cart');
    }
    
    public function addPartToCart(Request $request)
    {
        $this->validate($request, [
            'part' => 'required|exists:parts,id',
            'quantity' => 'required|min:1|numeric'
        ]);
        $part = Part::find($request->part);
        $this->cart->add($part, $request->quantity);
        return redirect()->back();
    }

    public function update(Request $request)
    {
        foreach ($request->cart as $item) {
            $this->cart->update($item['rowId'], (int)$item['qty']);
        }
        return redirect()->back();
    }

    public function delete($id = null)
    {
        if ($id) {
            $this->cart->remove($id);
        }
        return redirect()->back();
    }

    public function order(PDF $pdf)
    {
        $now = Carbon::now();
        $filename = "cotizacion" . '-' . Auth::user()->name . '-' . $now->format('Y-m-d');
        $user = \Auth::user();
        
        $pdf = $pdf->loadView('pdfs.cotizacion', [
            'cart' => $this->cart,
            'user' => $user,
            'now'  => $now
        ])->save(public_path("/cotizaciones/{$filename}.pdf"));

        Mail::send('emails.order', [], function ($message) use ($filename, $user) {
            $message->from('us@example.com', 'Laravel');
            $message->subject('Cotización International');
            $message->attach(public_path("/cotizaciones/{$filename}.pdf"));
            $message->to($user->email)->bcc(env('CORREO_NUEVA_COTIZACION'));
        });

        $this->cart->destroy();

        return redirect()->route('cart.success');
    }

    public function success()
    {
        return view('success');
    }
}
