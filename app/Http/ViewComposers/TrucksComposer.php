<?php

namespace App\Http\ViewComposers;

use App\TruckFamily;
use Illuminate\View\View;

class TrucksComposer
{

    public function compose(View $view)
    {
        $families = TruckFamily::with(['trucks' => function($query) {
            $query->where('status', true);
        }])->where('status', true)->get();
        $view->with('families', $families);
    }
}