<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Tavo\ValidadorEc;

class LegalId implements Rule
{
    protected $validator;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->validator = new ValidadorEc();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->validator->validarCedula($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Número de cédula inválido';
    }
}
