<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material', 18);
            $table->string('descripcion', 191);
            $table->string('marca', 40);
            $table->string('grupo', 40);
            $table->boolean('prom')->default(false);
            $table->boolean('disp_q')->default(true);
            $table->boolean('disp_c')->default(true);
            $table->boolean('disp_g')->default(true);
            $table->decimal('precio', 15, 2);
            $table->decimal('precio_prom', 15, 2)->nullable();
            $table->string('unidad', 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
