<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->mediumText('description');
            $table->string('load_amount');
            $table->string('price')->nullable();
            $table->text('specs')->nullable();
            $table->string('specs_image')->nullable();
            $table->text('dimensions')->nullable();
            $table->string('dimensions_image')->nullable();
            $table->text('accessories')->nullable();
            $table->string('accessories_image')->nullable();
            $table->string('featured_image')->nullable();
            $table->string('catalog')->nullable();
            $table->boolean('status')->default(true);
            $table->unsignedInteger('truck_family_id');
            $table->foreign('truck_family_id')
                    ->references('id')->on('truck_families')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucks');
    }
}
