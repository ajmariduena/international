<?php

Auth::routes();

Route::get('{page}', 'FrontEndController@page');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:Administrador']], function () {
    Route::resource('users', 'UsersController');
    Route::resource('pages', 'PagesController');
    Route::resource('trucks', 'TrucksController');
    Route::resource('components', 'ComponentsController');
    Route::get('components/{type}/{id}/create', 'ComponentsController@create')->name('components.create');

    /* Parts */
    Route::get('parts/images', 'PartsController@filemanager')->name('parts.filemanager');
    Route::get('imagenes/conex', 'PartsController@getConnection')->name('imagenes.conex');
    Route::post('imagenes/conex', 'PartsController@postConnection')->name('imagenes.conex');

    Route::resource('resources', 'ResourcesController', [
      'only' => ['store', 'destroy']
  ]);
});
