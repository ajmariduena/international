<?php

Route::get('/', 'FrontEndController@home');

Route::redirect('/admin', '/admin/users');

Route::get('/camiones/{truck}', 'FrontEndController@truck')->name('truck');
Route::get('/repuestos/resultados', 'FrontEndController@search')->name('search');
Route::get('/repuestos/promocion', 'FrontEndController@onsale')->name('onsale');
Route::get('/repuestos/{part}', 'FrontEndController@part')->name('part');
Route::get('/carrito', 'CartController@cart')->name('cart');
Route::post('/parttocart', 'CartController@addPartToCart')->name('addPartToCart');
Route::post('/cart/update', 'CartController@update')->name('cart.update');
Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');
Route::get('/cart/order', 'CartController@order')->name('cart.order')->middleware('auth');

Route::get('/success', 'CartController@success')->name('cart.success')->middleware('auth');

/* Routes related with admin */
include('admin.php');
