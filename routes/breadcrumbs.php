<?php

Breadcrumbs::register('admin', function ($breadcrumbs) {
  $breadcrumbs->push('Admin', url('/admin'));
});

/* Pages */
Breadcrumbs::register('pages', function ($breadcrumbs) {
  $breadcrumbs->parent('admin');
  $breadcrumbs->push('Páginas', route('pages.index'));
});

Breadcrumbs::register('create-page', function ($breadcrumbs) {
  $breadcrumbs->parent('pages');
  $breadcrumbs->push('Crear', route('pages.index'));
});

Breadcrumbs::register('page', function ($breadcrumbs, $page) {
  $breadcrumbs->parent('pages');
  $breadcrumbs->push($page->title, route('pages.edit', $page));
});

/* Components */
Breadcrumbs::register('component', function ($breadcrumbs, $type, $component) {
  $breadcrumbs->parent(strtolower($type->model_name), $type);
  $breadcrumbs->push($component->title, route('components.edit', $component));
});

Breadcrumbs::register('create-component', function ($breadcrumbs, $type, $name) {
  $breadcrumbs->parent(strtolower($type->model_name), $type);
  $breadcrumbs->push($name);
});

/* Users */
Breadcrumbs::register('users', function ($breadcrumbs) {
  $breadcrumbs->parent('admin');
  $breadcrumbs->push('Usuarios', route('users.index'));
});

Breadcrumbs::register('create-user', function ($breadcrumbs) {
  $breadcrumbs->parent('users');
  $breadcrumbs->push('Crear');
});

Breadcrumbs::register('edit-user', function ($breadcrumbs, $user) {
  $breadcrumbs->parent('users');
  $breadcrumbs->push($user->name);
});


/* Users */
Breadcrumbs::register('trucks', function ($breadcrumbs) {
  $breadcrumbs->parent('admin');
  $breadcrumbs->push('Camiones', route('trucks.index'));
});

Breadcrumbs::register('create-truck', function ($breadcrumbs) {
  $breadcrumbs->parent('trucks');
  $breadcrumbs->push('Crear');
});

Breadcrumbs::register('edit-truck', function ($breadcrumbs, $truck) {
  $breadcrumbs->parent('trucks');
  $breadcrumbs->push($truck->name);
});


/* Parts */
Breadcrumbs::register('parts', function ($breadcrumbs) {
  $breadcrumbs->parent('admin');
  $breadcrumbs->push('Repuestos');
});

Breadcrumbs::register('images', function ($breadcrumbs) {
  $breadcrumbs->parent('parts');
  $breadcrumbs->push('Imágenes');
});