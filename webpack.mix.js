let mix = require('laravel-mix');
var tailwindcss = require('tailwindcss');

// mix.js('resources/assets/js/app.js', 'public/js')
//     .browserSync('international.dev')
//     .sass('resources/assets/sass/app.scss', 'public/css');

// mix.js('resources/assets/cms/js/app.js', 'public/js/cms')
//     //.browserSync('international.dev')
//     .copyDirectory('resources/assets/cms/js/views/', 'public/js/cms')
//     .sass('resources/assets/cms/sass/app.scss', 'public/css/cms');

mix.sass('resources/assets/sass/cart.scss', 'public/css')
    .options({
    processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.js') ],
    });